<?php
date_default_timezone_set('UTC+3'); //устанавливаю часовой пояс по умочанию
$time = date('d.m.Y H:i:s'); //объявляем переменную со знвчением текущей даты и времени для лога
function logging($msg, $logfile = 'xmlparse.txt'){
	file_put_contents($logfile, $msg.PHP_EOL, FILE_APPEND);
}
if (file_exists('result.csv')){
	unlink('result.csv');
	logging("$time result.csv is deleted");
}
$fp = fopen (dirname(__FILE__) . '/test.xml', 'w+');
$ch = curl_init('https://ef.icontext.ru/employment/test.xml');
curl_setopt($ch, CURLOPT_TIMEOUT, 50);
curl_setopt($ch, CURLOPT_FILE, $fp); 
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_exec($ch); 
curl_close($ch);
fclose($fp);

logging ("test.xml is downloaded");

$localxml = simplexml_load_file('test.xml');
function getCategoryById($categoryId, $xml)	
{
	$list = $xml->xpath("//shop/categories/category[@id={$categoryId}]");
	return $list[0];
}
foreach ($localxml->shop->offers->offer as $product) {
	$stringCats = "";
	$cat = getCategoryById($product->categoryId, $localxml);
	while ($cat["parentId"] != "") {
		$stringCats.= $cat;
		$stringCats.= " / ";
		$cat = getCategoryById($cat["parentId"], $localxml);
	}
	file_put_contents(__DIR__.'/result.csv', 
			implode("\t", [
				$product->name , 
				$product->price.$product->currencyId,
				$product->description,
				getCategoryById($product->categoryId, $localxml),
				$stringCats,
				]).PHP_EOL, 
		FILE_APPEND); 
}